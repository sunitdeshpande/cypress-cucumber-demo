/// <reference types="cypress" />
declare namespace Cypress {
    interface Chainable {
        waitUntilVisible(locator: string, timeout?: number): void;

        appData(key: string): Cypress.Chainable<any>;
    }
}