Feature: Todo MVC

  Scenario: Adding new task
    Given I visit todo mvc
    When I add a new task "item1"
    Then I verify "item1" is added as task

  Scenario: Test json path works
    Then jsonpath should work