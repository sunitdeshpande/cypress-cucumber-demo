import {Given, Then, When} from "cypress-cucumber-preprocessor/steps";
import * as jsonpath from 'jsonpath';

import TodoMVCPage from "../../pages/TodoMVCPage";

Given('I visit todo mvc', () => {
    TodoMVCPage.visit();
});

When('I add a new task {string}', (dataKey: string) => {
    TodoMVCPage.addTask(dataKey);
});

Then('I verify {string} is added as task', (dataKey: string) => {
   TodoMVCPage.expect().toHaveTask(dataKey);
});

Then('jsonpath should work', () => {
    const cities = [
        { name: "London", "population": 8615246 },
        { name: "Berlin", "population": 3517424 },
        { name: "Madrid", "population": 3165235 },
        { name: "Rome",   "population": 2870528 }
    ];

    const names = jsonpath.query(cities, '$..name');
    expect(names).to.deep.equal(["London", "Berlin", "Madrid", "Rome" ]);
});