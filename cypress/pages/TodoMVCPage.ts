const TASK_INPUT = 'input.new-todo';
const TASK_INPUT_XPATH = '//input[@class="new-todo"]';
const TASK_ITEM = 'ul.todo-list label';

export default class TodoMVCPage {
    static visit() {
        cy.visit('/');
    }

    static addTask(dataKey: string) {
        // cy.get(TASK_INPUT, { timeout: 10000 }).should('be.visible');
        cy.waitUntilVisible(TASK_INPUT);

        cy.appData(dataKey).then((item) => {
            cy.xpath(TASK_INPUT_XPATH).type(`${item}{enter}`);
        })

    }

    static expect() {
        return {
            toHaveTask: (dataKey: string) => {

                cy.appData(dataKey).then((item) => {
                    cy.get(TASK_ITEM).contains(item).should('exist');
                });
            }
        }
    }
}